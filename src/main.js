import { createApp } from 'vue'
// import 'element-plus/dist/index.css'
import './assets/sass/overrideElementPlus.sass'
import ElementPlus from 'element-plus'
import App from './App.vue'
import router from './router'
import store from './store';
import axios from 'axios';
import VueAxios from 'vue-axios';

const app = createApp(App)

app.use(ElementPlus)
app.use(router)
app.use(store)
app.use(VueAxios, axios);
app.provide('axios', axios);

app.mount('#app')
