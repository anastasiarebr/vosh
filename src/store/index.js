import { createStore } from 'vuex';
import axios from 'axios';

export default createStore({
  state: {
    contestant: {
      user: {
        surname: "",
        name: "",
        patronymic: "",
        sex: "",
        phone: "",
        birthday: "",
        email: "",
        password: ""
      },
      document: {
        type: "",
        series: "",
        number: "",
        date_issue: "",
        authority: ""
      },
      address: {
        region: "",
        zone: "",
        city: ""
      },
      photo: '',
      school: {
        full_name: "",
        short_name: "",
        status_olympic: "",
        mark_olympic: "",
        grade_number: "",
        grade_letter: "",
        grade_performs: "",
        fio_teacher: "",
        place_work: "",
        post: "",
        academic_degree: ""
      },
      additional: {
        size: "",
        is_allergy: 1,
        note_allergy: "",
        is_voz: 1,
        is_need_conditions: 1,
        note_conditions: "",
        note_special: ""
      },
      agree_file: {
        name: ""
      },
      accompanying_persons: {
        list: []
      }
    },
    accompanying: {
      fio: "",
      sex: "",
      birthday: "",
      place_birth: "",
      phone: "",
      email: "",
      place_work: "",
      post: "",
      agree_file: ''
    },
    accList: [
      {
        id: '0',
        name: 'Иванов Иван Иванович',
        post: 'Старший преподаватель'
      },
      {
        id: '1',
        name: 'Попов Арсений Сергеевич',
        post: 'Старший преподаватель'
      },
      {
        id: '3',
        name: 'Александр Сергеевич Пушкин',
        post: 'Старший преподаватель'
      },
      {
        id: '4',
        name: 'Петров Петр Петрович',
        post: 'Старший преподаватель'
      },
    ]
  },
  mutations: {
    SET_USER (state, user) {
      state.contestant.user = user;
    },
    SET_DOCUMENT (state, document) {
      state.contestant.document = document;
    },
    SET_ADDRESS (state, address) {
      state.contestant.address = address;
    },
    SET_PHOTO (state, photo) {
      state.contestant.photo = photo;
    }
  },
  actions: {
    SET_PESONAL_DATA ({commit}, data) {
      commit('SET_USER', data.user);
      commit('SET_DOCUMENT', data.document);
      commit('SET_ADDRESS', data.address);
      commit('SET_PHOTO', data.photo);
    }
  },
  getters: {
    accList(state) {
      return state.accList
    },
    isMoscow(state) {
      const region = state.contestant.address.region.toLowerCase();
      const city = state.contestant.address.city.toLowerCase();
      return region.includes('москва') || city.includes('москва');
    }
  }
})